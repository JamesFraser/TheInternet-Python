# The-Internet Selenium Framework

A simple Python framework for running Selenium tests against The-Internet: https://the-internet.herokuapp.com/

## Getting Started

Install Python 3

Create a virtual environment (optional but recommended)
https://virtualenv.pypa.io/en/stable/
```
pip install virtualenv
virtualenv venv
```
Windows: `venv\Scripts\activate`
Mac or Linux: `source venv/bin/activate`

Install dependencies 
`pip install -r requirements.txt`


The framework is split into three packages: Core, Model and Tests.  

### Core
Used to handle the logic of the framework - creating/tearing down the Driver, handling config, etc.

### Model
Used to represent the pages and controls found in the site and handling how the driver can interact with them. These are typically split into directories for each page or a common directory if a control appears on multiple pages.  

### Tests
Where the actual tests and test configs should be placed. 

## Model

### Controls
Used to represent any objects that are typically repeated (such as shopping catalogue items) or complex (such as search controls) in order to encapsulate their logic and keep the model clean and simple to use. 

In order to create a control object, you must extend the `BaseControl` class in order to provide access to the driver (any common functionality such as IsVisible(), etc can be added here). The constructor should take in a WebDriver object and, in the case of repeated controls, a WebElement container object. The container is used to restrict the driver to the current control when finding elements. 

### Pages
In order to create a page object, you must extend the `BasePage` class in order to provide access to the driver. The constructor should take in a WebDriver object.

### Locators
Keeping all page/control locators in an internal class keeps refactoring in the future simple. This could be moved to a series of external class files if the number of locators begins to grow.

## Tests

### Changing Test Settings
Tests can be run in a variety of ways, this framework currently only supports local runs on Firefox, but it would be simple to add additional browser types or to run on SauceLabs or a Selenium Grid etc. To make use of these (if implemented), go to the `execution/configuration.py` file in the `Tests` package and update this section

```
set_environment_variables(
    {
        TestEnvironment: ExecutionEnvironment.Local,
        TestBrowser: Browser.Firefox,
    }
)
```
to use the correct values. This can be deleted for use in pipeline deployments where the environment variables are set by the deployment system.

### Creating Tests
To add a new set of tests, you must extend the `BaseTestCase` class, which gives you access to the driver and handles the setup and teardown of the tests.
Tests can then be created using the Page Object Model accessible by the base class. Test names and classes must begin with a "test_" prefix to be picked up by the Python test runner.
```
def test_login_successful_with_valid_credentials(self):
	login_page = LoginPage(self.driver).load()
	login_page.login_with_credentials('username', 'password')
	self.assertTrue(login_page.is_logged_in(), 'Login failed with valid credentials')
```

### Running tests
Tests can be executed by running `python -m unittest discover src/tests/` from the root directory.
