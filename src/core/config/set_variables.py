import os

TestEnvironment = 'TestEnvironment'
TestBrowser = 'TestBrowser'


def set_environment_variables(values):
    for variable_name, variable_value in values.items():
        if variable_value:
            set_environment_variable(variable_name, variable_value)


def get_environment_variable(name):
    return os.getenv(name)


def set_environment_variable(name, value):
    os.environ[name] = str(value)
