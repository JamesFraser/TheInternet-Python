class InvalidBrowserException(Exception):
    def __init__(self, browser, msg=None):
        if msg is None:
            msg = "Browser '{}' was not recognised.".format(browser)
        super(InvalidBrowserException, self).__init__(msg)
        self.browser = browser
