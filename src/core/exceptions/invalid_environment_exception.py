class InvalidEnvironmentException(Exception):
    def __init__(self, environment, msg=None):
        if msg is None:
            msg = "Environment '{}' was not recognised.".format(environment)
        super(InvalidEnvironmentException, self).__init__(msg)
        self.environment = environment
