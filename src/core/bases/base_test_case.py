from unittest import TestCase

from src.core.driver.webdriver_factory import WebDriverFactory


class BaseTestCase(TestCase):

    def setUp(self):
        self.driver = WebDriverFactory.get()

    def tearDown(self):
        self.driver.quit()