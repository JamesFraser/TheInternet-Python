from selenium import webdriver

from src.core.config.browser import Browser
from src.core.config.execution_environment import ExecutionEnvironment
from src.core.config.set_variables import *
from src.core.exceptions.invalid_browser_exception import InvalidBrowserException
from src.core.exceptions.invalid_environment_exception import InvalidEnvironmentException


class WebDriverFactory:

    @staticmethod
    def get():
        browser = get_environment_variable(TestBrowser)
        environment = get_environment_variable(TestEnvironment)

        if environment == ExecutionEnvironment.Local:
            return WebDriverFactory.get_local_driver(browser)

        raise InvalidEnvironmentException(environment)

    @staticmethod
    def get_local_driver(browser):
        if browser == Browser.Firefox:
            return webdriver.Firefox()

        raise InvalidBrowserException(browser)
