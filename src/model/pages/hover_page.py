from selenium.webdriver.common.by import By

from src.core.bases.base_page import BasePage
from src.model.controls.hovers_page.figure import Figure


class HoverPage(BasePage):

    @property
    def figures(self):
        elements = self.driver.find_elements(*self.Locators.Figure)
        return [Figure(self.driver, element) for element in elements]

    def load(self):
        self.driver.get('https://the-internet.herokuapp.com/hovers')
        return self

    class Locators:
        Figure = (By.CSS_SELECTOR, '.figure')