from selenium.webdriver.common.by import By

from src.core.bases.base_page import BasePage


class LoginPage(BasePage):

    @property
    def username_inputbox(self):
        return self.driver.find_element(*self.Locators.UsernameInputbox)

    @property
    def password_inputbox(self):
        return self.driver.find_element(*self.Locators.PasswordInputbox)

    @property
    def login_button(self):
        return self.driver.find_element(*self.Locators.LoginButton)

    @property
    def login_failure_banner_shown(self):
        return len(self.driver.find_elements(*self.Locators.LoginFailureBanner)) > 0

    def load(self):
        self.driver.get('https://the-internet.herokuapp.com/login')
        return self

    def is_logged_in(self):
        return len(self.driver.find_elements(*self.Locators.LoginSuccessBanner)) > 0

    def login_with_credentials(self, username, password):
        self.username_inputbox.clear()
        self.username_inputbox.send_keys(username)

        self.password_inputbox.clear()
        self.password_inputbox.send_keys(password)

        self.login_button.click()

    class Locators:
        UsernameInputbox = (By.ID, 'username')
        PasswordInputbox = (By.ID, 'password')
        LoginButton = (By.CSS_SELECTOR, 'button[type="submit"]')
        LoginFailureBanner = (By.CSS_SELECTOR, '#flash.flash.error')
        LoginSuccessBanner = (By.CSS_SELECTOR, '#flash.flash.success')