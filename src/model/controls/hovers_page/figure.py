from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from src.core.bases.base_control import BaseControl


class Figure(BaseControl):

    def __init__(self, driver, container):
        super().__init__(driver)
        self.container = container

    def hover_over(self):
        ActionChains(self.driver).move_to_element(self.container).perform()

    @property
    def name(self):
        self.hover_over()
        return self.container.find_element(*self.Locators.NameLabel).text[6:]

    def go_to_profile(self):
        self.hover_over()
        self.container.find_element(*self.Locators.ProfileAnchor).click()

    class Locators:
        ProfileAnchor = (By.CSS_SELECTOR, 'a')
        NameLabel = (By.CSS_SELECTOR, 'h5')
