from src.core.config.browser import Browser
from src.core.config.execution_environment import ExecutionEnvironment
from src.core.config.set_variables import set_environment_variables, TestEnvironment, TestBrowser

set_environment_variables(
    {
        TestEnvironment: ExecutionEnvironment.Local,
        TestBrowser: Browser.Firefox,
    }
)
