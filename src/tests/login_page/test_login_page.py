from src.core.bases.base_test_case import BaseTestCase
from src.model.pages.login_page import LoginPage


class TestLoginPage(BaseTestCase):

    def test_login_successful_with_valid_credentials(self):
        login_page = LoginPage(self.driver).load()
        login_page.login_with_credentials('tomsmith', 'SuperSecretPassword!')
        self.assertTrue(login_page.is_logged_in(), 'Login failed with valid credentials')

    def test_login_fails_with_invalid_username(self):
        login_page = LoginPage(self.driver).load()
        login_page.login_with_credentials('invalid', 'SuperSecretPassword!')
        self.assertFalse(login_page.is_logged_in(), 'Login succeeded with invalid username')

    def test_login_fails_with_invalid_password(self):
        login_page = LoginPage(self.driver).load()
        login_page.login_with_credentials('tomsmith', 'invalid')
        self.assertFalse(login_page.is_logged_in(), 'Login succeeded with invalid password')
