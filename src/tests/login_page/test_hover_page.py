from src.core.bases.base_test_case import BaseTestCase
from src.model.pages.hover_page import HoverPage


class TestHoverPage(BaseTestCase):

    def test_expected_number_of_figures(self):
        hover_page = HoverPage(self.driver).load()
        self.assertEqual(3, len(hover_page.figures),
                         'There were not 3 figures shown as expected.')

    def test_expected_username(self):
        hover_page = HoverPage(self.driver).load()
        self.assertEqual("user1", hover_page.figures[0].name,
                         'The first figure did not have the correct username.')
